package ru.aston.trainee.telegramservice.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.aston.trainee.telegramservice.config.TelegramBot;
import ru.aston.trainee.telegramservice.kafka.KafkaProducer;
import ru.aston.trainee.telegramservice.kafka.dto.TelegramTokenDto;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class TelegramUpdateProcessor {

    private final TelegramBot telegramBot;
    
    private final KafkaProducer kafkaProducer;

    public static final Map<Long, Byte> SCENERY_MAP = new HashMap<>();
    
    public static final byte CHECK_TOKEN = 1;
    public static final byte ALREADY_REGISTERED = 2;

    public TelegramUpdateProcessor(TelegramBot telegramBot, KafkaProducer kafkaProducer) {
        this.telegramBot = telegramBot;
        this.kafkaProducer = kafkaProducer;
    }

    public void processUpdate(Update update) {
        if (update == null) {
            log.warn("Received update is null");
            return;
        }
        if (update.hasMessage()) {
            checkMessageType(update);
        } else {
            log.warn("Unsupported message type is received: {}", update);
        }
    }

    private void checkMessageType(Update update) {
        Message message = update.getMessage();
        if (message.hasText()) {
            processTextMessage(update);
        } else {
            setUnsupportedMessageTypeView(update);
        }
    }

    private void setUnsupportedMessageTypeView(Update update) {
        SendMessage botMessage = MessageGenerator.generateSendMessageFromUpdate(update, "Неподдерживаемый тип сообщения!");
        send(botMessage);
    }

    public void send(SendMessage botMessage) {
        telegramBot.sendAnswerMessage(botMessage);
    }

    private void processTextMessage(Update update) {
        Long chatId = update.getMessage().getChatId();
        String userMessage = update.getMessage().getText();
        if (!SCENERY_MAP.containsKey(chatId)) {
            SCENERY_MAP.put(chatId, CHECK_TOKEN);
            send(MessageGenerator.generateSendMessageFromUpdate(
                    update,
                    "Здравствуйте! Отправте токен, что вы получили от сотрудника банка, в следующем сообщении для " +
                            "идентификации вас в системе и возможности получать уведомления в дальнейшем")
            );
        }else {
            Byte action = SCENERY_MAP.get(chatId);
            switch (action) {
                case CHECK_TOKEN -> {
                    send(MessageGenerator.generateSendMessageFromUpdate(update, "Проверяю токен..."));
                    kafkaProducer.tokenVerificationRequest(new TelegramTokenDto(userMessage, chatId));
                }
                case ALREADY_REGISTERED -> send(MessageGenerator.generateSendMessageFromUpdate(
                        update, "Вы уже зарегистрированы, дополнительные действия не требуются")
                );
            }
        }
    }
}
