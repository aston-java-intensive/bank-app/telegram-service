package ru.aston.trainee.telegramservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;

@SpringBootApplication
public class TelegramServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelegramServiceApplication.class, args);
	}

}
